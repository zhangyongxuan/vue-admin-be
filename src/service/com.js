const moment = require('moment');

const comDao = require('../dao/com');
const { mysqlTest, queryCity, queryCityByPid } = comDao;

const utils = require('../util');
const { sendEMail, generateTree } = utils;

const { redis } = require('../dbconnector');

const EMAIL_REDIS_KEY = 'EMAIL_REDIS_KEY';

const sendCode = async (ctx) => {
  const {
    request: {
      query: { emailUrl: sendToEmail = '1095056580@qq.com' },
    },
    hostname,
  } = ctx;
  let code = Math.random().toFixed(6) * 1000000;
  let res;
  try {
    const a = await redis.get('a');
    const isCodeExits = await redis.exists(`EMAIL_REDIS_KEY_${hostname}`);
    if (isCodeExits === 1) {
      code = await redis.get(`EMAIL_REDIS_KEY_${hostname}`);
    }
    await redis.set(`EMAIL_REDIS_KEY_${hostname}`, code);
    await redis.expire(`EMAIL_REDIS_KEY_${hostname}`, 300);
    res = await sendEMail(code, sendToEmail);
    if (res) {
      return {
        code: 200,
        msg: '发送成功',
      };
    }
  } catch (error) {
    redis.del(`EMAIL_REDIS_KEY_${hostname}`);
    res = error;
    throw new Error(error);
  }
};

const getUsers = async () => {
  const res = await mysqlTest();
  return res;
};

const queryCityService = async () => {
  try {
    const citys = await queryCity();
    let cityTree = [];
    // cityTree = citys;
    cityTree = generateTree(citys, 0, {
      p_fields: 'parent_code',
      c_fields: 'code',
    });
    return cityTree;
  } catch (error) {
    throw new Error(error);
  }
};
const queryCityByPidService = async (pid) => {
  try {
    const citys = await queryCityByPid(pid);

    return citys;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  sendCode,
  getUsers,
  queryCityService,
  queryCityByPidService,
};
