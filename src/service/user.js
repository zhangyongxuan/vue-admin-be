const comDao = require('../dao/user');
const { addUser } = comDao;

const { encryption, genId } = require('../util');

const addUserService = async (ctx) => {
  const {
    request: { body = {} },
  } = ctx;
  const { userName, pwd } = body;
  const userInfor = {
    user_name: userName,
    pwd: encryption(pwd),
    id: genId(),
  };
  try {
    const res = await addUser(userInfor);
    return res;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  addUserService,
};
