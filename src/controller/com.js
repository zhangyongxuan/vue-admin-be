const Router = require('koa-router');

const router = new Router({ prefix: process.env.API_PREFIX + '/com' }); // 注册地址

const {
  sendCode,
  getUsers,
  queryCityService,
  queryCityByPidService,
} = require('../service/com');
const { minio } = require('../dbconnector');

const { insertFileURL } = require('../dao/com');

router.get('/send-code', async (ctx) => {
  try {
    const res = await sendCode(ctx);
    ctx.response.body = res;
  } catch (error) {
    console.log(error);
    ctx.response.body = { code: 500, msg: '发送失败' };
  }
});
router.get('/get-users', async (ctx) => {
  try {
    const res = await getUsers();
    ctx.response.body = { code: 200, data: res };
  } catch (error) {
    ctx.response.body = { code: 500, msg: error };
  }
});
router.get('/city-tree', async (ctx) => {
  try {
    const res = await queryCityService();
    ctx.response.body = { code: 200, data: res };
  } catch (error) {
    console.log(error);
    ctx.response.body = { code: 500, msg: error };
  }
});
router.get('/city-tree-children', async (ctx) => {
  try {
    const {
      request: {
        query: { pid },
      },
    } = ctx;
    if (!pid || typeof pid !== 'string') {
      throw new Error('参数错误');
    }
    const res = await queryCityByPidService(pid);
    ctx.response.body = { code: 200, data: res };
  } catch (error) {
    ctx.response.body = { code: 500, msg: error.toString() };
  }
});

router.post('/upload', async (ctx) => {
  try {
    const {
      request: {
        files: { file },
      },
    } = ctx;
    if (!file) {
      ctx.status = 500;
      ctx.response.body = {
        code: -1,
        msg: '文件不为空',
      };
      return;
    }
    const url = await minio.fPutObject('vue-admin', file);
    insertFileURL(url);
    ctx.response.body = {
      code: 200,
      data: url,
    };
  } catch (error) {
    ctx.response.body = {
      code: -1,
      msg: error.toString(),
    };
  }
});

// 导出
module.exports = router;
