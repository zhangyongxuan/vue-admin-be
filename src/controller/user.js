const Router = require('koa-router');

const userService = require('../service/user');
const { addUserService } = userService;

const { minio } = require('../dbconnector');

const router = new Router({ prefix: process.env.API_PREFIX + '/user' }); // 注册地址
// 相当于 '/users/login'
router.post('/add-user', async (ctx) => {
  try {
    const res = await addUserService(ctx);
    ctx.status = 200;
    ctx.response.body = res;
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.response.body = {
      code: 500,
      msg: error,
    };
  }
});
router.post('/uploadAvatar', async (ctx) => {
  try {
    const {
      request: {
        files: { file },
      },
    } = ctx;
    if (!file) {
      ctx.status = 500;
      ctx.response.body = {
        code: -1,
        msg: '文件不为空',
      };
      return;
    }
    const buckets = await minio.fPutObject('vue-admin', file);
    ctx.response.body = {
      code: 200,
      data: buckets,
    };
  } catch (error) {
    ctx.response.body = {
      code: -1,
      msg: error.toString(),
    };
  }
});

// 导出
module.exports = router;
