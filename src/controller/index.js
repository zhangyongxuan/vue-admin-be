const userRoutes = require('./user');
const comRoutes = require('./com');

module.exports = [userRoutes, comRoutes];
