const MySQL = require('./mysql');
const Redis = require('./redis');
const Minio = require('./minio');
module.exports = {
  // mysql: new MySQL(),
  redis: new Redis().getRedisClient(),
  minio: new Minio(),
};
