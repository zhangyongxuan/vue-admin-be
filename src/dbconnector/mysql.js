var mysql = require('mysql');

class MySql {
  constructor() {
    // 1. 创建连接
    this.connection = mysql.createConnection({
      host: '49.234.34.27',
      user: 'root',
      password: '123456',
      database: 'mydb',
    });
    //   2. 连接数据库
    this.connection.connect((err) => {
      if (err) {
        throw new Error(err);
      }
    });
  }
  execSql(sql) {
    return new Promise((resolve, reject) => {
      // 3. 执行数据操作
      this.connection.query(sql, function (error, results, fields) {
        if (error) reject(error);
        else {
          // console.log('The solution is: ', results, fields);
          resolve(results);
        }
      });
    });
  }
  closeConntecton() {
    // 4. 关闭连接
    this.connection.end();
  }
}

module.exports = MySql;
