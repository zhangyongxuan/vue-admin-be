const redis = require('redis');

// 创建连接终端

class Redis {
  constructor() {
    //redis[s]://[[username][:password]@][host][:port][/db-number]
    this.connection = redis.createClient({
      url: 'redis://49.234.34.27:6379/1',
    });
    this.connection.on('error', (err) => {
      throw new Error(err);
    });
    this.connection.connect();
  }
  setKey(key, val) {
    if (typeof val === 'object') {
      val = JSON.stringify(val);
    }
    const res = this.connection.set(key, val);
    return res;
  }
  getKey(key) {
    return new Promise((resolve, reject) => {
      this.connection.get(key, (err, val) => {
        // 出错
        if (err) {
          reject(err);
          return;
        }
        // 值为空
        if (val == null) {
          resolve(null);
          return;
        }
        // 如果是json则转为对象，否则直接返回设置的值
        try {
          resolve(JSON.parse(val));
        } catch (ex) {
          resolve(val);
        }
      });
    });
  }
  setExpireTime(key, time) {
    this.connection.expire(key, time);
  }
  getRedisClient() {
    return this.connection;
  }
}

module.exports = Redis;
