const minio = require('minio');

class Minio {
  constructor() {
    this.minioClient = new minio.Client({
      endPoint: 'server.zhangyongxuan.com',
      port: 8082,
      useSSL: true,
      accessKey: 'admin',
      secretKey: 'admin123',
    });
  }
  getClient() {
    return this.minioClient;
  }
  // 获取所有桶名
  listBuckets() {
    return new Promise((resolve, reject) => {
      this.minioClient.listBuckets((err, buckets) => {
        if (err) {
          reject(err);
        } else {
          resolve(buckets);
        }
      });
    });
  }

  fPutObject(bucketName, file) {
    const { filepath, mimetype, newFilename, originalFilename } = file;
    const metaData = {
      'Content-Type': mimetype,
    };
    const fileName = newFilename + originalFilename;
    return new Promise((resolve, reject) => {
      this.minioClient.fPutObject(
        bucketName,
        fileName,
        filepath,
        metaData,
        (error, objInfo) => {
          if (error) {
            reject(error);
          } else {
            this.minioClient.presignedGetObject(
              bucketName,
              fileName,
              (err, url) => {
                if (err) {
                  reject(err);
                } else {
                  resolve(url.split('?')[0]);
                }
              }
            );
          }
        }
      );
    });
  }
}
module.exports = Minio;
