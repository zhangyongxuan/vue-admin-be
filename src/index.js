const Koa = require('koa');
const path = require('path');
const koaStatic = require('koa-static');
const bodyParser = require('koa-body');
const app = new Koa();

const routes = require('./controller');
const { transformDataKey } = require('./util');

// 转换post请求参数 可通过ctx.request.body获取json数据 可通过ctx.request.files获取formData中的文件流
app.use(
  bodyParser({
    multipart: true,
    formidable: {
      maxFileSize: 200 * 1024 * 1024, // 设置上传文件大小最大限制，默认2M
    },
  })
);
// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});
// 设置允许跨域
app.use(async (ctx, next) => {
  await next();
  const allowHost = ['49.234.34.27:8201'];
  const {
    request: { host },
  } = ctx;

  if (allowHost.includes(host)) {
    ctx.set('Access-Control-Allow-Origin', host);
  }
});
// logger
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  const {
    request: { host },
  } = ctx;
  console.log(`${host}---${ctx.method} ${ctx.url} - ${ms}`);
});

// 数据库key转为驼峰式
app.use(async (ctx, next) => {
  await next();
  if (ctx.response.body) {
    const { code, data } = ctx.response.body;
    if (code === 200 && Array.isArray(data) && data.length > 0) {
      const newData = transformDataKey(data);
      ctx.response.body = { code, data: newData };
    }
  }
});

routes.forEach((item) => {
  app.use(item.routes());
  app.use(item.allowedMethods());
});

app.listen(8202);
