const { mysql } = require('../dbconnector');

const mysqlTest = async () => {
  const res = await mysql.execSql('select * from sys_user');
  return res;
};

const queryCity = async () => {
  try {
    const citys = await mysql.execSql('select * from sys_lc_area');
    return citys;
  } catch (error) {
    throw new Error(error);
  }
};
const queryCityByPid = async (pid) => {
  try {
    const citys = await mysql.execSql(
      `select * from sys_lc_area where parent_code=${pid}`
    );
    return citys;
  } catch (error) {
    throw new Error(error);
  }
};
const insertFileURL = async (url) => {
  try {
    const citys = await mysql.execSql(
      `INSERT INTO files (url) VALUES ('${url}')`
    );
    return citys;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  mysqlTest,
  queryCity,
  queryCityByPid,
  insertFileURL,
};
