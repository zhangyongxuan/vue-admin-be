const { mysql } = require('../dbconnector');

// INSERT INTO sys_user (id,user_name,pwd) VALUES(1212312,'zyx','13123123')
const addUser = async (data = {}) => {
  const keyStr = Object.keys(data).join(',');
  const valueStr = Object.values(data)
    .map((i) => `'${i}'`)
    .join(',');
  try {
    const res = await mysql.execSql(
      `INSERT INTO sys_user (${keyStr}) VALUES (${valueStr})`
    );
    return res;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  addUser,
};
