const lodash = require('lodash');

const { sendEMail } = require('./email');
const snowflake = require('./snowflake');
const jsencrypt = require('./jsencrypt');

const transformDataKey = (data = []) => {
  const result = [];

  data.forEach((i) => {
    const o = {};
    for (const key in i) {
      if (Object.hasOwnProperty.call(i, key)) {
        o[lodash.camelCase(key)] = i[key];
      }
    }
    if (i.children && i.children.length > 0) {
      o.children = transformDataKey(i.children);
    }
    result.push(o);
  });

  return result;
};

const generateTree = (list = [], pid = 0, fields = {}) => {
  const { p_fields = 'id', c_fields = 'pid' } = fields;
  const tree = [];
  list.forEach((item) => {
    if (item[p_fields] === pid) {
      item.children = generateTree(list, item[c_fields], fields);
      tree.push(item);
    }
  });
  return tree;
};

module.exports = {
  sendEMail,
  transformDataKey,
  generateTree,
  genId: snowflake,
  ...jsencrypt,
};
