/**
 * @author kjprime
 * @description 发送邮件的封装
 */

const nodemailer = require('nodemailer');
//fkqvybcojdjndhie

const config = {
  // 这个地方是一个smtp服务器地址，一般都是smtp + xxx 比如网易地址 smtp.163.com 如果不行就百度搜索xxx的smtp
  host: 'smtp.qq.com',
  // 这个就是对应端口
  port: 465,
  secure: true,
  // 开启ssl
  secureConnection: true,
  auth: {
    user: '1966674622@qq.com', // 账号
    pass: 'fkqvybcojdjndhie', //授权码或者密码
  },
};

const server = nodemailer.createTransport(config);

let mail = {
  from: config.auth.user,
  to: '',
  html: '',
  subject: '邮箱验证', //邮件标题
};
/**
 * 发送邮件函数封装
 * @param {string} sendContent 发送的验证码或者内容
 * @param {string}} sendToEmail 发送到的email地址
 * @returns promise
 */
const sendEMail = (sendContent, sendToEmail) => {
  return new Promise((res, rej) => {
    mail['html'] = `<div style="
                          display: flex;
                          justify-content: center;
                          flex-direction: column;
                          align-items: center;">
                        <h1 style="color:#5888F7">ADMIN</h1>
                        <div style="color:#F75860">验证码是：${sendContent}</div>
                        <div style="color:#F75860">5分钟之内有效</div>
                     </div>`;
    mail['to'] = sendToEmail;
    server.sendMail(mail, function (err, msg) {
      //回调函数
      if (err) {
        console.log('发送失败');
        rej(err);
      } else {
        console.log('发送成功');
        res(msg);
      }
    });
  });
};

/**
 * @description 测试
 */
module.exports = {
  sendEMail,
};
