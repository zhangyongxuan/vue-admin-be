/**
 * 加密部分
 * @module utils/jsencrypt
 */

// 密钥对生成 http://web.chacuo.net/netrsakeypair
const bcryptjs = require('bcryptjs');
const NodeRSA = require('node-rsa');

const nodeRsa = new NodeRSA({ b: 1024 });
nodeRsa.setOptions({ encryptionScheme: 'pkcs1' });
const encrypt = (data) => {
  return nodeRsa.encrypt(data, 'base64');
};
const decrypt = (data) => {
  return nodeRsa.decrypt(data, 'utf8');
};

//加密
const encryption = (txt) => bcryptjs.hashSync(txt, 10);
//验证  txt:明文 password:密文
const validatePassword = (txt, password) => bcryptjs.compareSync(txt, password);

module.exports = {
  encryption,
  validatePassword,
  encrypt,
  decrypt,
  nodeRsa,
};
