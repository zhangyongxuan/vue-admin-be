// This file is created by egg-ts-helper@1.30.4
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportCusmw = require('../../../app/middleware/cusmw');
import ExportVerifyToken = require('../../../app/middleware/verifyToken');

declare module 'egg' {
  interface IMiddleware {
    cusmw: typeof ExportCusmw;
    verifyToken: typeof ExportVerifyToken;
  }
}
