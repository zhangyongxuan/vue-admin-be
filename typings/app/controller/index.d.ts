// This file is created by egg-ts-helper@1.30.4
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportCommon = require('../../../app/controller/common');
import ExportMap = require('../../../app/controller/map');
import ExportUser = require('../../../app/controller/user');
import ExportWeixin = require('../../../app/controller/weixin');

declare module 'egg' {
  interface IController {
    common: ExportCommon;
    map: ExportMap;
    user: ExportUser;
    weixin: ExportWeixin;
  }
}
